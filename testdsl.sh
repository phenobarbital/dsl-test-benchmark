#!/usr/bin/env bash
# check download speed rate via wget and curl    Date: 2013-05-12
# authors:
# Copyright: 2013 Jesús Lara <jesuslarag@gmail.com>
# speedtest.py
# Copyright: 2012 Janis Jansons (janis.jansons@janhouse.lv)
#
# 
# License: GPL v.3
# requires: curl, python-argparse, python-lxml, dig

# changelog
# (get latest version from "https://bitbucket.org/phenobarbital/dsl-test-benchmark")
#
# 2013-05-12 : speedtest 

# use
VERBOSE="true"
LOGFILE="log/dsltest.log"

# logo de ABA (92kb)
CANTV='http://www.cantv.com.ve/Portales/Cantv/plantilla2007/banner_home_ofic.gif'
OIFS="$IFS"
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# load message functions
if [ -e /usr/lib/dsl-test-benchmark/messages.sh ]; then
    . /usr/lib/dsl-test-benchmark/messages.sh
else
    . ./lib/messages.sh
fi

# colectando informacion basica
# IP
info "1.- Obteniendo informacion basica"
IP=`curl -s icanhazip.com`
# determinando el puntero *reverso* de tu IP (te dira por donde sales)
PTR=`dig @8.8.8.8 -x $IP +noall +answer | tail -n1 | awk '{print $5}'`
debug "Mi IP: $IP"
debug "Mi PTR: $PTR"
echo

TEST="$DIR/lib/speedtest-cli"
if [ -f $TEST ]; then
	# ejecutando speed test (calcula tu proveedor de velocidad mas cercano)
	info "2.- Ejecutando pruebas de speedtest, espere unos segundos"
	IFS=$'\n'
	SPEEDTEST=($(python $TEST --share))
	IFS="$OIFS"
	# parsing results
	ISP=`echo ${SPEEDTEST[2]} | cut -d '(' -f1 | sed 's/Testing from //g'`
	SERVER=`echo ${SPEEDTEST[4]} | cut -d ':' -f1 | sed 's/Hosted by //g'`
	LATENCY=`echo ${SPEEDTEST[4]} | cut -d ':' -f2 | cut -d ' ' -f2`
	PNG=`echo ${SPEEDTEST[9]} | cut -d ' ' -f3`
	DOWNLOAD=`echo ${SPEEDTEST[6]} | cut -d ' ' -f2`
	UPLOAD=`echo ${SPEEDTEST[8]} | cut -d ' ' -f2`	
	debug "Download: $DOWNLOAD Mbit/s"
	debug "Upload: $UPLOAD Mbit/s"
	echo
else
	error "Error: no hemos encontrado speedtest-cli ¿podrias revisar el fuente que descargaste?"
	exit 1
fi
# determinar velocidad de descarga de un archivo de 1MB desde USA
info "3.- Determinando la velocidad de descarga de archivo de 1MB desde USA"

LEASEWEB='http://mirror.leaseweb.net/speedtest/1mb.bin'
OVH='http://proof.ovh.net/files/1Mio.dat'

# curl test
ovhtest=`curl --progress-bar -w "%{speed_download}" -o /dev/null $OVH | tr ',' '.'`
OVH=$(LANG=C; echo "scale=2; $ovhtest / 131072" | bc)
echo "$OVH Mbit/s"

# test pre-transfer, lookup time, starxfer time:
info "4.- probando la velocidad de busqueda, resolucion y conexion a un servidor remoto"

# tiempo que tarda en activar la conexion (lookup time)
lookuptest=`curl --progress-bar -w "%{time_namelookup}" -o /dev/null $CANTV | tr ',' '.'`
LOOKUP=$(LANG=C; echo "$lookuptest")
echo "$LOOKUP seg"

# tiempo que tarda en realizar la conexion
connectest=`curl --progress-bar -w '%{time_starttransfer}-%{time_pretransfer}' -o /dev/null $CANTV | tr ',' '.'`
CONNECT=$(LANG=C; echo "$connectest"|bc)
echo "$CONNECT seg"

# tiempo total de la descarga
totaltime=`curl --progress-bar -w "%{time_total}" -o /dev/null $CANTV | tr ',' '.'`
TOTALTIME=$(LANG=C; echo "$totaltime")
echo "$TOTALTIME seg"

info " ---- Resultados ----- "
echo "Tu IP publica: $IP"
echo "La direccion de tu proveedor: $PTR"
echo "Tu proveedor parece ser: $ISP"
echo " = Prueba de SpeedTest ="
echo "Servidor mas cercano (basado en ping): $SERVER"
echo "Latencia: $LATENCY ms"
echo "Download: $DOWNLOAD Mbit/s"
echo "Upload: $UPLOAD Mbit/s"
warning "Comparte tus resultados: $PNG"
echo " = Descarga de Archivos de 1MB desde USA = "
echo "Descarga desde OVH: $OVH MB/s"
echo " = Tiempos de resolucion, conexion y descarga (92Kb) = "
echo "Tiempo de resolucion, busqueda y conexion: $LOOKUP seg"
echo "Tiempo de inicio de transferencia: $CONNECT seg"
echo "Tiempo Total descarga (92Kb): $TOTALTIME seg"
info " ----------------------- "

